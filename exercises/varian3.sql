CREATE DATABASE Varian3

USE Varian3

CREATE TABLE Customers
(
Id INT,
Name VARCHAR(50),
Email VARCHAR(50),
Birthday DATETIME,
CONSTRAINT PK_Customers PRIMARY KEY(Id)
)

CREATE TABLE Destinations
(
Id INT,
Name VARCHAR(50),
Price SMALLMONEY,
Duration INT,
CONSTRAINT PK_Destinations_Id PRIMARY KEY(Id),
CONSTRAINT CHK_Duration CHECK (Duration >= 1)
)

CREATE TABLE Reservations
(
Id INT,
TimeReservation DATETIME,
CustomerId INT,
DestinationId INT,
CONSTRAINT PK_Reservations_Id PRIMARY KEY(Id),
CONSTRAINT FK_Reservations_Customers FOREIGN KEY(CustomerId)
REFERENCES Customers(Id),
CONSTRAINT FK_Reservations_Destinations FOREIGN KEY(DestinationId)
REFERENCES Destinations(Id)
)

ALTER TABLE Customers
ADD Phone VARCHAR(10)

INSERT INTO Customers(Id, Name, Email, Birthday, Phone)
VALUES (1, 'Valentin','obbittoo@gmail.com', '2/14/1994', '0090000000');

INSERT INTO Destinations(Id, Price, Duration)
VALUES (1, 12.00, 2);

INSERT INTO Reservations(Id, TimeReservation, CustomerId, DestinationId)
VALUES (1, GETDATE(), 1, 1);

UPDATE Destinations 
SET Duration = 10, Price = 1500
WHERE Id = 1
 
DELETE FROM Destinations 
WHERE Name LIKE 'P%' AND Price < 700;

USE ShopDb

--01
SELECT p.NAME, p.DESCR FROM PRODUCTS AS p
INNER JOIN ORDER_ITEMS AS oi
ON oi.PRODUCT_ID = p.PRODUCT_ID
WHERE oi.UNIT_PRICE > 100
GROUP BY p.Name, p.DESCR
ORDER BY p.Name ASC

--02
SELECT j.JOB_TITLE, MAX_SALARY FROM JOBS AS j
WHERE MAX_SALARY IS NOT NULL
ORDER BY j.MAX_SALARY ASC

--03
SELECT SUM(oi.UNIT_PRICE * oi.QUANTITY) FROM ORDERS AS o
INNER JOIN ORDER_ITEMS AS oi
ON o.ORDER_ID = oi.ORDER_ID
WHERE o.ORDER_DATE > CONVERT(DATETIME, '03.01.1999', 104)





